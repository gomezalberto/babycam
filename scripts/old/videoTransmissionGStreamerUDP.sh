# Capture video and stream to a socket
# Options used:
#	-t, --timeout	: Time (in ms) to capture for. If not specified, set to 5s. Zero to disable
#	-b, --bitrate	: Set bitrate. Use bits per second (e.g. 10MBits/s would be -b 10000000)
#	-o, --output	: Output filename <filename> (to write to stdout, use '-o -').
#		  Connect to a remote IPv4 host (e.g. tcp://192.168.1.2:1234, udp://192.168.1.2:1234)
#		  To listen on a TCP port (IPv4) and wait for an incoming connection use -l
#		  (e.g. raspivid -l -o tcp://0.0.0.0:3333 -> bind to all network interfaces, raspivid -l -o tcp://192.168.1.1:3333 -> bind to a certain local IPv4)

serverIp=$(ifconfig | grep -E 'inet.[0-9]' | grep -v '127.0.0.1' | awk '{ print $2}')
clientIp=$(echo $serverIp | cut -d '.' -f 1-3).255 # Send to all

# Send captured audio
#gst-launch-1.0 -v alsasrc device=plughw:Device ! mulawenc ! rtppcmupay ! udpsink host=$clientIp port=5002 &
#gst-launch-1.0 -v alsasrc device=plughw:Device ! mulawenc ! rtppcmupay ! tcpserversink host=$serverIp port=5001 &

# I send the stream to stdout, then capture with a pipe and use gstreamer to send to network
raspivid -t 0 -w 1080 -h 720 -fps 25 -b 2000000 -o - | gst-launch-1.0 -v fdsrc ! h264parse ! rtph264pay  ! udpsink host=$serverIp port=5000
#raspivid -t 0 -w 1080 -h 720 -fps 25 -b 2000000 -o - | gst-launch-1.0 -v fdsrc ! h264parse ! rtph264pay config-interval=1 pt=96  ! gdppay ! udpsink host=$clientIp port=5000
