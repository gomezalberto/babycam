# WIP #


Setting up Raspberry Pi 3
I started following the tutorial on https://kamranicus.com/guides/raspberry-pi-3-baby-monitor
This tutorial is great but the result has a significant lag (~5 seconds). 
Video can be captured with the raspvid command. Audio can be sent with gstreamer.
Instructions for this are available here. With these settings, I can stream audio and video with decent quality and little delay, as follows:
# Transmit bash script #
serverIp=$(ifconfig | grep -E 'inet.[0-9]' | grep -v '127.0.0.1' | awk '{ print $2}')
clientIp=$(echo $serverIp | cut -d '.' -f 1-3).255 # Send to all
VPORT=5000
APORT=5002
# Video settings ------------
#w=1280; h=720; #  HD
w=640; h=480;
FPS="15/1"
# Send captured audio
gst-launch-1.0 -v alsasrc device=plughw:Device ! mulawenc ! rtppcmupay ! udpsink host=$clientIp port=$APORT &
## I send the stream to stdout, then capture with a pipe and use gstreamer to send to network
gst-launch-1.0 rpicamsrc ! 'video/x-h264, width='$w', height='$h', framerate='$FPS',profile=high' ! queue ! h264parse ! rtph264pay config-interval=1 pt=96  ! gdppay ! tcpserversink host=$serverIp port=$VPORT
# Receive  bash #
serverIp=babypi.local
gst-launch-1.0 -v udpsrc port=5002 caps="application/x-rtp" ! queue ! rtppcmudepay ! mulawdec ! audioconvert ! autoaudiosink syn
c=false &
gst-launch-1.0 -v tcpclientsrc host=$serverIp port=5000 ! gdpdepay ! rtph264depay ! avdec_h264 ! videoconvert ! autovideosink sy
nc=false
kill $!
Alternatively, and for performance, the stream can be sent and received in UDP. In my experience, this works best if the viewer device is specifically targeted by transmitting to its ip address rather than to broadcast (X.X.X.255)
Now, in order to have an app that can see all this, we can use gstreamer. To use it on Android Studio there is a good tutorial to have the examples run.
To make it start atomatically, we add a process to init.d and register it as described in this tutorial. Particularly, to prevent it from starting too early, do 
1.5: things to do
Combine both streams into a stream that has both audio and video and can be loaded with an app.
Sudo service uv4l_raspicam stop
2. Tried and failed:
I started following the tutorial on https://kamranicus.com/guides/raspberry-pi-3-baby-monitor
This tutorial is great but the result has a significant lag (~5 seconds). 
That needs to be then setup to be launched on start.
Gstreamer gst-launch-1.0
Also check this solution which uses a gst wrapper to raspivid on this repo
The simplest gstreamer pieline for video would be:
gst-launch-1.0 videotestsrc ! autovideosink
In python
Which opens a test video stream and pipes it to standard video sink.
Useful resources: 
https://hmbd.wordpress.com/2016/08/01/raspberry-pi-video-and-audio-recording-and-streaming-guide/
UDP streaming with low latency
Real time video only  (https://elinux.org/RPi-Cam-Web-Interface)
2. Capturing video/audio and streaming it in real time:
# Capture video and stream to a socket
# Options used:
#    -t, --timeout    : Time (in ms) to capture for. If not specified, set to 5s. Zero to disable
#    -b, --bitrate    : Set bitrate. Use bits per second (e.g. 10MBits/s would be -b 10000000)
#    -o, --output    : Output filename <filename> (to write to stdout, use '-o -').
#   	   Connect to a remote IPv4 host (e.g. tcp://192.168.1.2:1234, udp://192.168.1.2:1234)
#   	   To listen on a TCP port (IPv4) and wait for an incoming connection use -l
#   	   (e.g. raspivid -l -o tcp://0.0.0.0:3333 -> bind to all network interfaces, raspivid -l -o tcp://192.168.1.1:3333 -> bind to a certain local IPv4)
serverIp=$(ifconfig | grep -E 'inet.[0-9]' | grep -v '127.0.0.1' | awk '{ print $2}')
clientIp=$(echo $serverIp | cut -d '.' -f 1-3).255 # Send to all
# Send captured audio
gst-launch-1.0 -v alsasrc device=plughw:Device ! mulawenc ! rtppcmupay ! udpsink host=$clientIp port=5002 &
# I send the stream to stdout, then capture with a pipe and use gstreamer to send to network
raspivid -t 0 -w 1080 -h 720 -fps 25 -b 2000000 -o - | gst-launch-1.0 -v fdsrc ! h264parse ! rtph264pay config-interval=1 pt=96 ! gdppay ! tcpserversink host=$serverIp port=5000
kill $!
That script creates two transmission sockets.
3. Receiving the stream using an external app (e.g. VLC)
Load network stream using the address:
Failed attempts:
Setting up media server/player:
 I just followed the tutorial using the basic settings here: https://elinux.org/RPi-Cam-Web-Interface
Setting up sound
By default, RPi cam web interface does not stream sound.
Setting up audio and video
