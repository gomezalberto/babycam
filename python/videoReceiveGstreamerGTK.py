#!/usr/bin/python
import gi
gi.require_version('Gst', '1.0')
gi.require_version('Gtk', '3.0')
from gi.repository import GObject
from gi.repository import Gst
from gi.repository import Gtk

HOST='babypi.local'
PORT=5000

GObject.threads_init()
Gst.init(None)



class Main:
    def __init__(self):
        # Create gui bits and bobs
		
        builder = Gtk.Builder()
        builder.add_from_file('sound.glade')
        self.window = builder.get_object("mainwindow")

        signals = {
            "on_play_clicked" : self.OnPlay,
            "on_stop_clicked" : self.OnStop,
            "on_quit_clicked" : self.OnQuit,
        }

        builder.connect_signals(signals)
        
        # Create GStreamer bits and bobs

        self.pipeline = Gst.Pipeline("receivePipeline")
        # The pipeline reflects this command: 
        # gst-launch-1.0 -v tcpclientsrc host=$serverIp port=5000 ! gdpdepay ! rtph264depay ! avdec_h264 ! videoconvert ! autovideosink sync=false
        
        # Create GStreamer elements
        self.tcpsrc = Gst.ElementFactory.make('tcpclientsrc', None)
        self.tcpsrc.set_property('port', PORT)
        self.tcpsrc.set_property('host', HOST)
        self.gdpdepay = Gst.ElementFactory.make('gdpdepay',None)
        self.rtdepay = Gst.ElementFactory.make('rtph264depay',None)
        self.avdec = Gst.ElementFactory.make('avdec_h264',None)
        self.vidconvert = Gst.ElementFactory.make('videoconvert',None)
        self.asink = Gst.ElementFactory.make('autovideosink', None)
        self.asink.set_property('sync', False)
        # Add elements to the pipeline
        self.pipeline.add(self.tcpsrc)
        self.pipeline.add(self.gdpdepay)
        self.pipeline.add(self.rtdepay)
        self.pipeline.add(self.avdec)
        self.pipeline.add(self.vidconvert)
        self.pipeline.add(self.asink)
        
        self.tcpsrc.link(self.gdpdepay)
        self.gdpdepay.link(self.rtdepay)
        self.rtdepay.link(self.avdec)
        self.avdec.link(self.vidconvert)
        self.vidconvert.link(self.asink)
        
        self.window.show_all()

    def OnPlay(self, widget):
        print("play")
        self.pipeline.set_state(Gst.State.PLAYING)

    def OnStop(self, widget):
        print("stop")
        #self.pipeline.set_state(Gst.State.READY)
        self.pipeline.set_state(Gst.State.PAUSED)

    def OnQuit(self, widget):
        Gtk.main_quit()

start=Main()
Gtk.main()


"""
Use with the following sound.glade file:

<?xml version="1.0" standalone="no"?> <!--*- mode: xml -*-->
<!DOCTYPE glade-interface SYSTEM "http://glade.gnome.org/glade-2.0.dtd">

<interface>

<object class="GtkWindow" id="mainwindow">
  <property name="visible">True</property>
  <property name="title" translatable="yes">window1</property>
  <property name="type">GTK_WINDOW_TOPLEVEL</property>
  <property name="window_position">GTK_WIN_POS_NONE</property>
  <property name="modal">False</property>
  <property name="resizable">True</property>
  <property name="destroy_with_parent">False</property>
  <property name="decorated">True</property>
  <property name="skip_taskbar_hint">False</property>
  <property name="skip_pager_hint">False</property>
  <property name="type_hint">GDK_WINDOW_TYPE_HINT_NORMAL</property>
  <property name="gravity">GDK_GRAVITY_NORTH_WEST</property>
  <property name="focus_on_map">True</property>
  <property name="urgency_hint">False</property>
  
  <child>
    <object class="GtkHButtonBox" id="hbuttonbox1">
      <property name="visible">True</property>
      <property name="layout_style">GTK_BUTTONBOX_DEFAULT_STYLE</property>
      <property name="spacing">0</property>

      <child>
	<object class="GtkButton" id="button1">
	  <property name="visible">True</property>
	  <property name="can_default">True</property>
	  <property name="can_focus">True</property>
	  <property name="label">gtk-media-play</property>
	  <property name="use_stock">True</property>
	  <property name="relief">GTK_RELIEF_NORMAL</property>
	  <property name="focus_on_click">True</property>
	  <signal name="clicked" handler="on_play_clicked" last_modification_time="Sat, 20 May 2006 22:35:59 GMT"/>
	</object>
      </child>

      <child>
	<object class="GtkButton" id="button2">
	  <property name="visible">True</property>
	  <property name="can_default">True</property>
	  <property name="can_focus">True</property>
	  <property name="label">gtk-media-stop</property>
	  <property name="use_stock">True</property>
	  <property name="relief">GTK_RELIEF_NORMAL</property>
	  <property name="focus_on_click">True</property>
	  <signal name="clicked" handler="on_stop_clicked" last_modification_time="Sat, 20 May 2006 22:36:08 GMT"/>
	</object>
      </child>

      <child>
	<object class="GtkButton" id="button3">
	  <property name="visible">True</property>
	  <property name="can_default">True</property>
	  <property name="can_focus">True</property>
	  <property name="label">gtk-quit</property>
	  <property name="use_stock">True</property>
	  <property name="relief">GTK_RELIEF_NORMAL</property>
	  <property name="focus_on_click">True</property>
	  <signal name="clicked" handler="on_quit_clicked" last_modification_time="Sat, 20 May 2006 22:36:21 GMT"/>
	</object>
      </child>
    </object>
  </child>
</object>

</interface>
"""
