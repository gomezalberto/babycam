#!/usr/bin/python
import gi
gi.require_version('Gst', '1.0')
gi.require_version('Gtk', '3.0')
from gi.repository import GObject
from gi.repository import Gst
from gi.repository import Gtk

HOST='babypi.local'
vPORT=5000 # video port
aPORT=5002 # audio port

GObject.threads_init()
Gst.init(None)



class Main:
    def __init__(self):
        # Create gui bits and bobs
		
        builder = Gtk.Builder()
        builder.add_from_file('sound.glade')
        self.window = builder.get_object("mainwindow")

        signals = {
            "on_play_clicked" : self.OnPlay,
            "on_stop_clicked" : self.OnStop,
            "on_quit_clicked" : self.OnQuit,
        }

        builder.connect_signals(signals)
        
        # Create GStreamer bits and bobs
        self.SetupVideoPipeline(HOST,vPORT)
        self.SetupAudioPipeline(HOST,aPORT)
        
        
        self.window.show_all()

    def OnPlay(self, widget):
        print("play")
        self.video_pipeline.set_state(Gst.State.PLAYING)
        self.audio_pipeline.set_state(Gst.State.PLAYING)

    def OnStop(self, widget):
        print("stop")
        #self.pipeline.set_state(Gst.State.READY)
        self.video_pipeline.set_state(Gst.State.PAUSED)
        self.audio_pipeline.set_state(Gst.State.PAUSED)

    def OnQuit(self, widget):
        Gtk.main_quit()

    def SetupVideoPipeline(self,host,port):
        """Setup GStreamer receive video pipeline.
        The pipeline should reflect this command
        gst-launch-1.0 -v tcpclientsrc host=$host port=$port \
            ! gdpdepay ! rtph264depay ! avdec_h264 ! videoconvert !\ 
            autovideosink sync=false 
        """
        self.video_pipeline = Gst.Pipeline("videoPipeline")
        # Create GStreamer elements
        self.video_src = Gst.ElementFactory.make('tcpclientsrc', None)
        self.video_src.set_property('port', port)
        self.video_src.set_property('host', host)
        self.gdpdepay = Gst.ElementFactory.make('gdpdepay',None)
        self.video_rtdepay = Gst.ElementFactory.make('rtph264depay',None)
        self.video_decoder = Gst.ElementFactory.make('avdec_h264',None)
        self.video_convert = Gst.ElementFactory.make('videoconvert',None)
        self.video_sink = Gst.ElementFactory.make('autovideosink', None)
        self.video_sink.set_property('sync', False)
        # Add elements to the pipeline
        self.video_pipeline.add(self.video_src)
        self.video_pipeline.add(self.gdpdepay)
        self.video_pipeline.add(self.video_rtdepay)
        self.video_pipeline.add(self.video_decoder)
        self.video_pipeline.add(self.video_convert)
        self.video_pipeline.add(self.video_sink)
        
        self.video_src.link(self.gdpdepay)
        self.gdpdepay.link(self.video_rtdepay)
        self.video_rtdepay.link(self.video_decoder)
        self.video_decoder.link(self.video_convert)
        self.video_convert.link(self.video_sink)
        
    def SetupAudioPipeline(self,host,port):
        """Setup GStreamer receive audio pipeline.
        The pipeline should reflect this command
        gst-launch-1.0 -v udpsrc port=5002 caps="application/x-rtp" ! \
            queue ! rtppcmudepay ! mulawdec ! audioconvert ! \
            autoaudiosink sync=false  
        """
        self.audio_pipeline = Gst.Pipeline("audioPipeline")
        # Create GStreamer elements
        self.audio_src = Gst.ElementFactory.make('udpsrc', None)
        self.audio_src.set_property('port', port)
        self.audio_src.set_property('caps', Gst.caps_from_string("application/x-rtp"))
        self.audio_queue = Gst.ElementFactory.make('queue',None)
        self.audio_rtdepay = Gst.ElementFactory.make('rtppcmudepay',None)
        self.audio_decoder = Gst.ElementFactory.make('mulawdec',None)
        self.audio_convert = Gst.ElementFactory.make('audioconvert',None)
        self.audio_sink = Gst.ElementFactory.make('autoaudiosink', None)
        self.audio_sink.set_property('sync', False)
        # Add elements to the pipeline
        self.audio_pipeline.add(self.audio_src)
        self.audio_pipeline.add(self.audio_queue)
        self.audio_pipeline.add(self.audio_rtdepay)
        self.audio_pipeline.add(self.audio_decoder)
        self.audio_pipeline.add(self.audio_convert)
        self.audio_pipeline.add(self.audio_sink)
        
        self.audio_src.link(self.audio_queue)
        self.audio_queue.link(self.audio_rtdepay)
        self.audio_rtdepay.link(self.audio_decoder)
        self.audio_decoder.link(self.audio_convert)
        self.audio_convert.link(self.audio_sink)
        
start=Main()
Gtk.main()


"""
Use with the following sound.glade file:

<?xml version="1.0" standalone="no"?> <!--*- mode: xml -*-->
<!DOCTYPE glade-interface SYSTEM "http://glade.gnome.org/glade-2.0.dtd">

<interface>

<object class="GtkWindow" id="mainwindow">
  <property name="visible">True</property>
  <property name="title" translatable="yes">window1</property>
  <property name="type">GTK_WINDOW_TOPLEVEL</property>
  <property name="window_position">GTK_WIN_POS_NONE</property>
  <property name="modal">False</property>
  <property name="resizable">True</property>
  <property name="destroy_with_parent">False</property>
  <property name="decorated">True</property>
  <property name="skip_taskbar_hint">False</property>
  <property name="skip_pager_hint">False</property>
  <property name="type_hint">GDK_WINDOW_TYPE_HINT_NORMAL</property>
  <property name="gravity">GDK_GRAVITY_NORTH_WEST</property>
  <property name="focus_on_map">True</property>
  <property name="urgency_hint">False</property>
  
  <child>
    <object class="GtkHButtonBox" id="hbuttonbox1">
      <property name="visible">True</property>
      <property name="layout_style">GTK_BUTTONBOX_DEFAULT_STYLE</property>
      <property name="spacing">0</property>

      <child>
	<object class="GtkButton" id="button1">
	  <property name="visible">True</property>
	  <property name="can_default">True</property>
	  <property name="can_focus">True</property>
	  <property name="label">gtk-media-play</property>
	  <property name="use_stock">True</property>
	  <property name="relief">GTK_RELIEF_NORMAL</property>
	  <property name="focus_on_click">True</property>
	  <signal name="clicked" handler="on_play_clicked" last_modification_time="Sat, 20 May 2006 22:35:59 GMT"/>
	</object>
      </child>

      <child>
	<object class="GtkButton" id="button2">
	  <property name="visible">True</property>
	  <property name="can_default">True</property>
	  <property name="can_focus">True</property>
	  <property name="label">gtk-media-stop</property>
	  <property name="use_stock">True</property>
	  <property name="relief">GTK_RELIEF_NORMAL</property>
	  <property name="focus_on_click">True</property>
	  <signal name="clicked" handler="on_stop_clicked" last_modification_time="Sat, 20 May 2006 22:36:08 GMT"/>
	</object>
      </child>

      <child>
	<object class="GtkButton" id="button3">
	  <property name="visible">True</property>
	  <property name="can_default">True</property>
	  <property name="can_focus">True</property>
	  <property name="label">gtk-quit</property>
	  <property name="use_stock">True</property>
	  <property name="relief">GTK_RELIEF_NORMAL</property>
	  <property name="focus_on_click">True</property>
	  <signal name="clicked" handler="on_quit_clicked" last_modification_time="Sat, 20 May 2006 22:36:21 GMT"/>
	</object>
      </child>
    </object>
  </child>
</object>

</interface>
"""
