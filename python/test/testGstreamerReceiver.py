from os import path

import gi
gi.require_version('Gst', '1.0')
gi.require_version('Gtk', '3.0')
gi.require_version('GstVideo', '1.0')
from gi.repository import GObject, Gst, Gtk

# Needed for window.get_xid(), xvimagesink.set_window_handle(), respectively:
from gi.repository import GdkX11, GstVideo


GObject.threads_init()
Gst.init(None)

HOST='192.168.1.47'
PORT=5000

class Player(object):
    def __init__(self):

        self.pipeline = Gst.Pipeline()

        self.tcpsrc = Gst.ElementFactory.make('tcpclientsrc',None)
        self.tcpsrc.set_property("host", HOST)
        self.tcpsrc.set_property("port", PORT)
        self.gdepay = Gst.ElementFactory.make('gdpdepay', None)
        self.rtdepay = Gst.ElementFactory.make('rtph264depay', None)
        self.avdec = Gst.ElementFactory.make('avdec_h264', None)
        self.vidconvert = Gst.ElementFactory.make('videoconvert', 'vidconvert')
        self.asink = Gst.ElementFactory.make('autovideosink', 'asink')
        self.asink.set_property('sync', False)
        #self.asink.set_property('emit-signals', True)
        #self.set_property('drop', True)

        self.pipeline.add(self.tcpsrc)
        self.pipeline.add(self.gdepay)
        self.pipeline.add(self.rtdepay)
        self.pipeline.add(self.avdec)
        self.pipeline.add(self.vidconvert)
        self.pipeline.add(self.asink)

        self.tcpsrc.link(self.gdepay)
        self.gdepay.link(self.rtdepay)
        self.rtdepay.link(self.avdec)
        self.avdec.link(self.vidconvert)
        self.vidconvert.link(self.asink)

    def run(self):

        self.pipeline.set_state(Gst.State.PLAYING)
        Gtk.main()

p = Player()
p.run()