#!/usr/bin/python
import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject
from gi.repository import Gst
from gi.repository import Gtk


GObject.threads_init()
Gst.init(None)

# Note: this is impossible to quit = use testStreamerGTK instead

class Main:
    def __init__(self):
        # Create gui bits and bobs
		
#        builder = Gtk.Builder()
 #       builder.add_from_file('sound.glade')
  #      self.window = builder.get_object("mainwindow")

        
        # Create GStreamer bits and bobs

        self.pipeline = Gst.Pipeline("mypipeline")
        self.videotestsrc = Gst.ElementFactory.make("videotestsrc", "video")
        self.pipeline.add(self.videotestsrc)
        self.sink = Gst.ElementFactory.make("autovideosink", "sink")
        self.pipeline.add(self.sink)
        self.videotestsrc.link(self.sink)
       # self.window.show_all()
        self.pipeline.set_state(Gst.State.PLAYING)



start=Main()
Gtk.main()



